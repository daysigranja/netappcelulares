﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppCelulares.entities
{
    class Celular
    {
        private int id;
        private RAM ram;
        private int camara = 32; //Megapixeles
        private int bateria = 4500; //Miliamperios
        private double pantalla = 6.7; //Pulgadas

        public Celular(int id, RAM ram, int camara, int bateria, double pantalla)
        {
            this.id = id;
            this.ram = ram;
            this.camara = camara;
            this.bateria = bateria;
            this.pantalla = pantalla;
        }

        public int Id { get => id; set => id = value; }
        public int Camara { get => camara; set => camara = value; }
        public int Bateria { get => bateria; set => bateria = value; }
        public double Pantalla { get => pantalla; set => pantalla = value; }
        internal RAM Ram { get => ram; set => ram = value; }

        public enum RAM
        {
            OCHOGB,SEISGB
        }

    }
}
