﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppCelulares
{
    public partial class FrmMain : Form
    {
        private DataSet DsSistema;
        public FrmMain()
        {
            DsSistema = new DataSet();
            InitializeComponent();
        }

        private void RegistrarVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistroVenta frv = new FrmRegistroVenta();
            frv.MdiParent = this;
            frv.Show();
        }

        private void InventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionCelulares fgc = new FrmGestionCelulares();
            fgc.MdiParent = this;
            fgc.Show();

        }
    }
}
