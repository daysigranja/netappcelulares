﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppCelulares
{
    public partial class FrmGestionCelulares : Form
    {
        private BindingSource bsCelulares = new BindingSource();
        public FrmGestionCelulares()
        {
            InitializeComponent();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            FrmRegistroVenta frv = new FrmRegistroVenta();
            frv.Show();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsCelulares.Filter = string.Format("Ram like '*{0}*'", txtBuscar.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
